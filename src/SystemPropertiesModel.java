final class SystemPropertiesModel {

	/* === System properties === */

	/** CLDC & MIDP platform properties. */
	public static final String[] CLDC_MIDP_SYSPROPS = {
		"microedition.platform",
		"microedition.configuration",
		"microedition.profiles",
		"microedition.jtwi.version",
		"microedition.msa.version",
		"microedition.locale",
		"microedition.encoding",
		"microedition.commports",
		"microedition.hostname",
		"device.manufacturer",
		"device.model",
		"software.version",
	};

	/**
	 * {@link javax.microedition.lcdui.Display} properties.
	 * This array is empty because does not have any property for use in
	 * {@link java.lang.System#getProperty(String) System.getProperty()}.
	 *
	 * @since 2.0
	 */
	public static final String[] LCDUI_DISPLAY_SYSPROPS = {};

	/**
	 * {@link javax.microedition.lcdui.Font} properties.
	 * This array is empty because does not have any property for use in
	 * {@link java.lang.System#getProperty(String) System.getProperty()}.
	 *
	 * @since 2.0
	 */
	public static final String[] LCDUI_FONTS_SYSPROPS = {};

	/**
	 * {@link javax.microedition.lcdui.Canvas} properties.
	 * This array is empty because does not have any property for use in
	 * {@link java.lang.System#getProperty(String) System.getProperty()}.
	 *
	 * @since 2.0
	 */
	public static final String[] LCDUI_CANVAS_SYSPROPS = {};

	/**
	 * FileConnection & PIM API (JSR 75) properties.
	 *
	 * @since 2.1
	 */
	public static final String[] FCPIM_SYSPROPS = {
		"microedition.pim.version",
		"microedition.io.file.FileConnection.version",
		"fileconn.dir.photos", "fileconn.dir.photos.name",
		"fileconn.dir.videos", "fileconn.dir.videos.name",
		"fileconn.dir.graphics", "fileconn.dir.graphics.name",
		"fileconn.dir.tones", "fileconn.dir.tones.name",
		"fileconn.dir.music", "fileconn.dir.music.name",
		"fileconn.dir.recordings", "fileconn.dir.recordings.name",
		"fileconn.dir.memorycard", "fileconn.dir.memorycard.name",
		"fileconn.dir.private", "fileconn.dir.private.name",
		"fileconn.dir.roots.name",
	};

	/** Bluetooth & OBEX API (JSR 82) properties. */
	public static final String[] BT_SYSPROPS = {
		"bluetooth.api.version",
		"bluetooth.l2cap.receiveMTU.max",
		"bluetooth.connected.devices.max",
		"bluetooth.connected.inquiry", "bluetooth.connected.inquiry.scan",
		"bluetooth.connected.page", "bluetooth.connected.page.scan",
		"bluetooth.master.switch",
		"bluetooth.sd.trans.max",
		"bluetooth.sd.attr.retrievable.max",
		"obex.api.version",
	};

	/** Wireless Messaging API (JSR 120 & 205) properties. */
	public static final String[] WMA_SYSPROPS = {
		"wireless.messaging.version",
		"wireless.messaging.sms.smsc",
		"wireless.messaging.mms.mmsc",
	};

	/** Mobile Media API (JSR 135) properties. */
	public static final String[] MMA_SYSPROPS = {
		"microedition.media.version",
		"supports.audio.capture", "supports.video.capture",
		"supports.mixing", "supports.recording",
		"audio.encodings", "video.encodings",
		"video.snapshot.encodings",
		"streamable.contents",
	};

	/**
	 * Web Services API (JSR 172) properties.
	 *
	 * @since 1.3
	 */
	public static final String[] WS_SYSPROPS = {
		"xml.jaxp.subset.version",
		"xml.rpc.subset.version",
	};

	/**
	 * Security and Trust Services API (JSR 177) properties.
	 *
	 * @since 1.3
	 */
	public static final String[] SATSA_SYSPROPS = {
		"microedition.smartcardslots",
		"microedition.satsa.apdu.version",
		"microedition.satsa.crypto.version",
		"microedition.satsa.pki.version",
	};

	/**
	 * Location API (JSR 179) properties.
	 *
	 * @since 1.3
	 */
	public static final String[] LOCATION_SYSPROPS = {
		"microedition.location.version",
		"fileconn.dir.landmarks", "fileconn.dir.landmarks.name",
	};

	/**
	 * Mobile 3D Graphics API (JSR 184) properties.
	 *
	 * @since 1.3
	 */
	public static final String[] M3G_SYSPROPS = {
		"microedition.m3g.version",
		"microedition.m3d.version",
	};

	/**
	 * Scalable Vector Graphics API (JSR 226 & 287) properties.
	 *
	 * @since 1.3
	 */
	public static final String[] SVG_SYSPROPS = {
		"microedition.m2g.version",
		"microedition.m2g.svg.version",
		"microedition.m2g.svg.baseProfile",
	};

	/**
	 * Advanced Multimedia Supplements API (JSR 234) properties.
	 *
	 * @since 1.3
	 */
	public static final String[] AMMS_SYSPROPS = {
		"microedition.amms.version",
		"supports.mediacapabilities",
		"tuner.modulations",
		"audio.samplerates",
		"audio3d.simultaneouslocations",
		"camera.orientations",
		"camera.resolutions",
	};

	/** OpenGL ES (JSR 239) properties. */
	public static final String[] OPENGL_SYSPROPS = {
		"microedition.khronos.egl.version",
		"microedition.khronos.opengles.version",
		"jsr239.noqueue",
		"jsr239.supportsEGL11",
		"jsr239.supportsGL11",
		"jsr239.supports_OES_query_matrix",
		"jsr239.supports_OES_draw_texture",
		"jsr239.supports_OES_matrix_palette",
		"jsr239.supports_OES_point_size",
		"jsr239.supports_OES_texture_cube_map",
		"jsr239.supports_OES_texture_env_crossbar",
		"jsr239.supports_OES_texture_mirrored_repeat",
		"jsr239.supports_OES_blend_subtract",
		"jsr239.supports_OES_blend_func_separate",
		"jsr239.supports_OES_blend_equation_separate",
		"jsr239.supports_OES_stencil_wrap",
		"jsr239.supports_OES_extended_matrix_palette",
		"jsr239.supports_OES_framebuffer_object",
	};

	/** Mobile Broadcast Service API (JSR 272) properties. */
	public static final String[] BROADCAST_SYSPROPS = {
		"microedition.broadcast.version",
		"microedition.broadcast.supports.overlay",
		"microedition.broadcast.supports.timedrecording",
		"microedition.broadcast.supports.filecache",
		"microedition.broadcast.supports.purchasing",
	};

	/** Other optional packages properties. */
	public static final String[] OTHERS_SYSPROPS = {
		"microedition.chapi.version",
		"microedition.contactless.version",
		"microedition.global.version",
		"microedition.payment.version",
		"microedition.sensor.version",
		"microedition.sip.version",
	};

	/**
	 * LG Electronics proprietary properties.
	 * <p>
	 * Ref.:
	 * <a href="http://developer.lge.com/resource/mobile/RetrieveOverview.dev?categoryTypeCode=JVRS#RS00000085">Introduction
	 * to LG Java Platform - LG Developer</a>.
	 */
	public static final String[] LGE_SYSPROPS = {
		"com.lge.batterylevel",
		"com.lge.display.indicator",
		"com.lge.display.indicator.default",
		"com.lge.display.rotation",
		"com.lge.flipopen",
		"com.lge.inlinevirtualkeypad",
		"com.lge.lcdui.oemfont",
		"com.lge.lgjp",
		"com.lge.lgjp.option",
		"com.lge.lgjp.specification",
		"com.lge.mgr.category",
		"com.lge.mgr.category.userfolder",
		"com.lge.run.type",
		"com.lge.virtualkeypad",
	};

	/**
	 * LG Electronics protected proprietary properties.
	 * <p>
	 * Ref.:
	 * <a href="http://developer.lge.com/resource/mobile/RetrieveOverview.dev?categoryTypeCode=JVRS#RS00000085">Introduction
	 * to LG Java Platform - LG Developer</a>.
	 */
	public static final String[] LGE_TRUSTED_SYSPROPS = {
		"com.lge.imei",
		"com.lge.imsi",
		"com.lge.net.cellid",
		"com.lge.net.cmcc",
		"com.lge.net.cmnc",
		"com.lge.net.isonhomeplmn",
		"com.lge.net.mcc",
		"com.lge.net.hmcc",
		"com.lge.net.mnc",
		"com.lge.net.hmnc",
		"com.lge.net.rat",
		"com.lge.net.rssi",
	};

	/** Nokia system proprietary properties. */
	public static final String[] NOKIA_SYSTEM_SYSPROPS = {
		"com.nokia.memoryramfree",
		"com.nokia.mid.batterylevel",
		"com.nokia.mid.calendars",
		"com.nokia.mid.countrycode",
		"com.nokia.mid.dateformat",
		"com.nokia.mid.timeformat",
		"com.nokia.gpu.memory.total",
		"com.nokia.gpu.memory.used",
		"com.nokia.gpu.memory.shared",
		"com.nokia.gpu.memory.private",
		"com.nokia.mid.cmdline",
		"com.nokia.mid.cmdline.instance",
	};

	/** Nokia File Connection package proprietary properties. */
	public static final String[] NOKIA_FILE_SYSPROPS = {
		"fileconn.dir.themes", "fileconn.dir.themes.name",
		"fileconn.dir.received", "fileconn.dir.received.name",
		"fileconn.dir.cache",
		"fileconn.dir.bookmarks",
		"fileconn.dir.applications.bookmarks",
		"fileconn.dir.games.bookmarks",
	};

	/** Nokia protected proprietary properties. */
	public static final String[] NOKIA_PROTECTED_SYSPROPS = {
		"Cell-ID",
		"com.nokia.mid.imei",
		"com.nokia.mid.imsi",
		"com.nokia.mid.spn",
		"com.nokia.mid.mnc",
		"com.nokia.mid.ons",
		"com.nokia.mid.networkID",
		"com.nokia.mid.cellid",
		"com.nokia.mid.lac",
		"com.nokia.mid.gid1",
		"com.nokia.mid.gid2",
		"com.nokia.mid.msisdn",
		"com.nokia.mid.productcode",
		"com.nokia.targetdebug",
	};

	/** Nokia Network proprietary properties. */
	public static final String[] NOKIA_NETWORK_SYSPROPS = {
		"com.nokia.mid.networkavailability",
		"com.nokia.mid.networkid",
		"com.nokia.mid.networksignal",
		"com.nokia.mid.networkstatus",
		"com.nokia.network.access",
		"com.nokia.midp.impl.isa.network.databearer",
	};

	/** Nokia Dual-SIM proprietary properties. */
	public static final String[] NOKIA_MULTISIM_SYSPROPS = {
		"com.nokia.multisim.cellid.sim1",
		"com.nokia.multisim.cellid.sim2",
		"com.nokia.multisim.lac.sim1",
		"com.nokia.multisim.lac.sim2",
		"com.nokia.multisim.mnc.sim1",
		"com.nokia.multisim.mnc.sim2",
		"com.nokia.multisim.networkavailability.sim1",
		"com.nokia.multisim.networkavailability.sim2",
		"com.nokia.multisim.networkID.sim1",
		"com.nokia.multisim.networkID.sim2",
		"com.nokia.multisim.networksignal.sim1",
		"com.nokia.multisim.networksignal.sim2",
		"com.nokia.multisim.networkstatus.sim1",
		"com.nokia.multisim.networkstatus.sim2",
	};

	/** Nokia UI package proprietary properties. */
	public static final String[] NOKIA_UI_SYSPROPS = {
		"com.nokia.mid.ui.version",
		"com.nokia.mid.ui.customfontsize",
		"com.nokia.mid.ui.frameanimator.fps",
		"com.nokia.mid.ui.frameanimator.pps",
		"com.nokia.mid.ui.joystick_event",
		"com.nokia.mid.ui.layout",
		"com.nokia.mid.ui.softnotification",
		"com.nokia.mid.impl.nativeDigitSupport",
		"com.nokia.mid.settings.ombActive",
		"com.nokia.keyboard.type",
		"com.nokia.key.scancode",
		"com.nokia.key.modifier",
	};

	/** Nokia UI package for S40 Full Touch proprietary properties. */
	public static final String[] NOKIA_UI_S40FT_SYSPROPS = {
		"com.nokia.mid.ui.screensaverprevention",
		"com.nokia.mid.ui.tactilefeedback",
		"com.nokia.mid.ui.multipointtouch.version",
		"com.nokia.mid.ui.gestures.version",
		"com.nokia.mid.ui.theme.version",
		"com.nokia.mid.ui.orientation.version",
		"com.nokia.mid.ui.locale.version",
	};

	/** Nokia Canvas UI package proprietary properties. */
	public static final String[] NOKIA_CANVAS_UI_SYSPROPS = {
		"com.nokia.canvas.net.indicator.location",
		"com.nokia.softkey1.label.anchor",
		"com.nokia.softkey1.label.location",
		"com.nokia.softkey2.label.anchor",
		"com.nokia.softkey2.label.location",
		"com.nokia.softkey3.label.anchor",
		"com.nokia.softkey3.label.location",
	};

	/**
	 * Sony Ericsson proprietary properties.
	 *
	 * @since 2.1
	 */
	public static final String[] SEMC_SYSPROPS = {
		"com.sonyericsson.java.platform",
		"com.sonyericsson.sim.subscribernumber",
		"com.sonyericsson.jackknifeopen",
		"com.sonyericsson.flipopen",
		"camera.mountorientation",
		"com.sonyericsson.active_profile",
		"com.sonyericsson.active_alarm",
		"com.sonyericsson.net.serviceprovider",
		"com.sonyericsson.net.networkname",
		"com.sonyericsson.net.mcc",
		"com.sonyericsson.net.mnc",
		"com.sonyericsson.net.cmcc",
		"com.sonyericsson.net.cmnc",
		"com.sonyericsson.net.isonhomeplmn",
		"com.sonyericsson.net.rat",
		"com.sonyericsson.net.cellid",
		"com.sonyericsson.net.lac",
		"com.sonyericsson.net.status",
		"com.sonyericsson.capuchin.version",
		"com.sonyericsson.capuchin.flash.version",
	};

	/**
	 * Property strings for gathering IMEI code.
	 *
	 * @since 1.3
	 */
	public static final String[] IMEI_SYSPROPS = {
		"IMEI",
		"phone.imei",
		"com.lge.imei",
		"com.motorola.IMEI",
		"com.nokia.IMEI",
		"com.nokia.mid.imei",
		"com.samsung.imei",
		"com.siemens.IMEI",
		"com.sonyericsson.imei",
	};

	/** Java standard platform properties. */
	public static final String[] JAVA_SYSPROPS = {
		"java.class.path",
		"java.class.version",
		"java.compiler",
		"java.ext.dirs",
		"java.home",
		"java.io.tmpdir",
		"java.library.path",
		"java.protocol.handler.pkgs",
		"java.vendor",
		"java.vendor.url",
		"java.version",
		"java.specification.name",
		"java.specification.vendor",
		"java.specification.version",
		"java.vm.name",
		"java.vm.vendor",
		"java.vm.version",
		"java.vm.specification.name",
		"java.vm.specification.vendor",
		"java.vm.specification.version",
		"file.encoding",
		"file.separator",
		"http.proxyHost",
		"http.proxyPort",
		"line.separator",
		"os.arch",
		"os.name",
		"os.version",
		"path.separator",
		"user.dir",
		"user.home",
		"user.language",
		"user.name",
		"user.region",
		"user.timezone",
		"user.variant",
	};

	/** Some application manifest properties. */
	public static final String[] ABOUT_SYSPROPS = {};

	/* === Category lists === */

	/** Java ME category strings */
	private static final String[] JAVAME_CATEGORY_STRINGS = {
		"CLDC & MIDP",
		"LCDUI Display",
		"LCDUI Fonts",
		"LCDUI Canvas & Events",
	};
	private static final String[][] JAVAME_CATEGORY_REFS = {
		CLDC_MIDP_SYSPROPS,
		LCDUI_DISPLAY_SYSPROPS,
		LCDUI_FONTS_SYSPROPS,
		LCDUI_CANVAS_SYSPROPS,
	};

	/** Java ME optional packages category strings */
	private static final String[] JAVAME_OPTIONAL_CATEGORY_STRINGS = {
		"File & PIM (JSR 75)",
		"Bluetooth & OBEX (JSR 82)",
		"WMA (JSR 120 & 205)",
		"Mobile Media (JSR 135)",
		"Web Services (JSR 172)",
		"Security & Trust Services (JSR 177)",
		"Location (JSR 179)",
		"Mobile 3D Graphics (JSR 184)",
		"SVG (JSR 226 & 287)",
		"AMMS (JSR 234)",
		"OpenGL ES (JSR 239)",
		"Mobile Broadcast Service (JSR 272)",
		"Other optional packages",
	};
	private static final String[][] JAVAME_OPTIONAL_CATEGORY_REFS = {
		FCPIM_SYSPROPS,
		BT_SYSPROPS,
		WMA_SYSPROPS,
		MMA_SYSPROPS,
		WS_SYSPROPS,
		SATSA_SYSPROPS,
		LOCATION_SYSPROPS,
		M3G_SYSPROPS,
		SVG_SYSPROPS,
		AMMS_SYSPROPS,
		OPENGL_SYSPROPS,
		BROADCAST_SYSPROPS,
		OTHERS_SYSPROPS,
	};

	/** Proprietary system properties category strings */
	private static final String[] PROPRIETARY_CATEGORY_STRINGS = {
		"LG",
		"LG (protected)",

		"Nokia (system)",
		"Nokia (file system)",
		"Nokia (protected)",
		"Nokia (network)",
		"Nokia (dual-SIM)",
		"Nokia (UI)",
		"Nokia (UI for S40 full touch)",
		"Nokia (UI Canvas)",

		"Sony Ericsson",
	};
	private static final String[][] PROPRIETARY_CATEGORY_REFS = {
		LGE_SYSPROPS,
		LGE_TRUSTED_SYSPROPS,

		NOKIA_SYSTEM_SYSPROPS,
		NOKIA_FILE_SYSPROPS,
		NOKIA_PROTECTED_SYSPROPS,
		NOKIA_NETWORK_SYSPROPS,
		NOKIA_MULTISIM_SYSPROPS,
		NOKIA_UI_SYSPROPS,
		NOKIA_UI_S40FT_SYSPROPS,
		NOKIA_CANVAS_UI_SYSPROPS,

		SEMC_SYSPROPS,
	};

	/** Main categories strings. */
	public static final String[] MAIN_CATEGORY_STRINGS = {
		"Standard Java ME",
		"Optional packages",
		"Brand specific",
		"IMEI",
		"Java platform",
		"About",
	};
	private static final String[][] MAIN_CATEGORY_REFS = {
		JAVAME_CATEGORY_STRINGS,
		JAVAME_OPTIONAL_CATEGORY_STRINGS,
		PROPRIETARY_CATEGORY_STRINGS,
		IMEI_SYSPROPS,
		JAVA_SYSPROPS,
		ABOUT_SYSPROPS,
	};

	/* === Constructors === */
	private SystemPropertiesModel() {
	}

	/* === Methods === */

	public static boolean hasSubcategory(final int index) {
		return index <= 2;
	}

	public static String[] getSubcategoryStrings(final int index) {
		if (!hasSubcategory(index)) {
			throw new ArrayIndexOutOfBoundsException("Subcategory not exists.");
		}
		return MAIN_CATEGORY_REFS[index];
	}

	public static String[] getSystemProperties(final int i, final int j) {
		switch (i) {
		case 0:
			return JAVAME_CATEGORY_REFS[j];
		case 1:
			return JAVAME_OPTIONAL_CATEGORY_REFS[j];
		case 2:
			return PROPRIETARY_CATEGORY_REFS[j];
		default:
			return MAIN_CATEGORY_REFS[i];
		}
	}
}
