import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Ticker;

final class TestCanvas extends Canvas implements CommandListener {
	private final Command fullscreenCommand
			= new Command("Fullscreen", Command.SCREEN, 0);
	private final Command toggleTickerCommand
			= new Command("Ticker", Command.SCREEN, 1);

	protected int colorBack = 0xFFFFFF;
	protected int colorFore = 0x000000;
	protected int colorBorder = 0x808080;
	protected int borderStyle = Graphics.DOTTED;

	protected int colorHLBack = 0x000000;
	protected int colorHLFore = 0xFFFFFF;
	protected int colorHLBorder = 0x808080;
	protected int borderStyleHL = Graphics.SOLID;

	private final Ticker ticker = new Ticker(
			"Press any key or tap on screen to view the response.");
	private boolean isFullscreenMode = false;

	private String sizeString;
	private final String doubleBufferedYesNo
			= toYesNoString(isDoubleBuffered());
	private final String repeatEventsYesNo
			= toYesNoString(hasRepeatEvents());
	private final String pointerEventsYesNo
			= toYesNoString(hasPointerEvents());
	private final String pointerMotionEventsYesNo
			= toYesNoString(hasPointerMotionEvents());
	private String pointerStatus = "Released";

	private final Hashtable keysPressed = new Hashtable(8);
	private final Vector pointsDragged = new Vector(128);
	private Point firstPoint, lastPoint;

	public TestCanvas(final Display display) {
		if (SystemPropertiesMIDlet.IS_MIDP_2) {
			colorBack = display.getColor(Display.COLOR_BACKGROUND);
			colorFore = display.getColor(Display.COLOR_FOREGROUND);
			colorBorder = display.getColor(Display.COLOR_BORDER);
			borderStyle = display.getBorderStyle(false);

			colorHLBack = display.getColor(
					Display.COLOR_HIGHLIGHTED_BACKGROUND);
			colorHLFore = display.getColor(
					Display.COLOR_HIGHLIGHTED_FOREGROUND);
			colorHLBorder = display.getColor(
					Display.COLOR_HIGHLIGHTED_BORDER);
			borderStyleHL = display.getBorderStyle(true);

			setTicker(ticker);
			addCommand(toggleTickerCommand);
			addCommand(fullscreenCommand);
		}
		setCommandListener(this);
	}

	private static final String toYesNoString(final boolean test) {
		return test ? "Yes" : "No";
	}

	protected void keyPressed(final int keyCode) {
		keysPressed.put(new Integer(keyCode), Boolean.FALSE);
		firstPoint = null;
		pointsDragged.removeAllElements();
		lastPoint = null;
		repaint();
	}

	protected void keyRepeated(final int keyCode) {
		final Object oldValue
				= keysPressed.put(new Integer(keyCode), Boolean.TRUE);
		if (!Boolean.TRUE.equals(oldValue)) {
			repaint();
		}
	}

	protected void keyReleased(final int keyCode) {
		keysPressed.remove(new Integer(keyCode));
		repaint();
		if (
			isFullscreenMode
			&& keyCode == KEY_STAR
			&& keysPressed.containsKey(KEY_POUND_OBJ)
			&& keysPressed.size() == 1
		) {
			setFullScreenMode(false);
			isFullscreenMode = false;
		}
	}
	private static final Integer KEY_POUND_OBJ = new Integer(KEY_POUND);

	protected void pointerPressed(final int x, final int y) {
		pointerStatus = "Pressed";
		firstPoint = new Point(x, y);
		pointsDragged.removeAllElements();
		lastPoint = null;
		repaint();
	}

	protected void pointerDragged(final int x, final int y) {
		final Point p = new Point(x, y);
		pointsDragged.addElement(p);
		if ("Dragged".equals(pointerStatus)) {
			repaint(p.x - 3, p.y - 3, 7, 7);
		} else {
			pointerStatus = "Dragged";
			repaint();
		}
	}

	protected void pointerReleased(final int x, final int y) {
		pointerStatus = "Released";
		lastPoint = new Point(x, y);
		repaint();
	}

	protected void sizeChanged(final int width, final int height) {
		sizeString = null;
		repaint();
	}

	protected void paint(final Graphics g) {
		final int width = getWidth();
		final int height = getHeight();

		g.setColor(colorBack);
		g.fillRect(0, 0, width, height);
		g.setColor(colorBorder);
		g.setStrokeStyle(borderStyle);
		g.drawRect(0, 0, width - 1, height - 1);

		if (!keysPressed.isEmpty()) {
			drawKeysPressed(g);
		} else {
			g.setColor(colorFore);
			if (sizeString == null) {
				sizeString = width + " × " + height;
			}
			final PropertiesDrawer pd
					= new PropertiesDrawer(g, 6, 3, width - 6);
			pd.drawProperty("Size: ", sizeString);
			pd.drawProperty("Double buffered: ", "Double buffer: ",
					doubleBufferedYesNo);
			pd.drawProperty("Key repeat events: ", "Key repeat evt.: ",
					repeatEventsYesNo);
			pd.drawProperty("Pointer events: ", pointerEventsYesNo);
			if (hasPointerEvents()) {
				pd.drawProperty("Pointer motion events: ", "P. motion evt.: ",
						pointerMotionEventsYesNo);
				pd.drawProperty("Pointer status: ", pointerStatus);
			}
			drawPointsPressed(g);
		}
	}

	private static final class PropertiesDrawer {
		private final Font textFont = Font.getDefaultFont();
		private final Font labelFont = Font.getFont(
				textFont.getFace(),
				textFont.getStyle() | Font.STYLE_BOLD,
				textFont.getSize());
		private final Graphics g;
		private final int x, width;
		private int y;

		public PropertiesDrawer(
			final Graphics g, final int x, final int y, final int width
		) {
			this.g = g;
			this.x = x;
			this.y = y;
			this.width = width;
		}

		public void drawProperty(final String label, final String text) {
			y += labelFont.getHeight();
			g.setFont(labelFont);
			g.drawString(label, x, y, Graphics.BASELINE | Graphics.LEFT);
			final int offset = labelFont.stringWidth(label);
			g.setFont(textFont);
			g.drawString(text, offset + x, y,
					Graphics.BASELINE | Graphics.LEFT);
		}

		public void drawProperty(
			final String longLabel, final String shortLabel,
			final String text
		) {
			if (labelFont.stringWidth(longLabel) <= width * 2 / 3) {
				drawProperty(longLabel, text);
			} else {
				drawProperty(shortLabel, text);
			}
		}
	}

	private void drawKeysPressed(final Graphics g) {
		final int width = getWidth();
		final int height = getHeight();
		final int size = keysPressed.size();
		int i = 0;

		final Enumeration e = keysPressed.keys();
		while (e.hasMoreElements()) {
			i++;

			g.setColor(colorHLBack);
			g.fillRoundRect(
					3, height * (i - 1) / size + 3,
					width - 6, height / size - 6,
					5, 5);

			g.setColor(colorHLBorder);
			g.setStrokeStyle(borderStyleHL);
			g.drawRoundRect(
					3, height * (i - 1) / size + 3,
					width - 7, height / size - 7,
					5, 5);

			final Integer keyCode = (Integer) e.nextElement();
			final StringBuffer keyName
					= new StringBuffer(getKeyName(keyCode.intValue()));
			switch (getGameAction(keyCode.intValue())) {
			case UP:
				keyName.append(" (Up)");
				break;
			case LEFT:
				keyName.append(" (Left)");
				break;
			case RIGHT:
				keyName.append(" (Right)");
				break;
			case DOWN:
				keyName.append(" (Down)");
				break;
			case FIRE:
				keyName.append(" (Fire)");
				break;
			case GAME_A:
				keyName.append(" (Game A)");
				break;
			case GAME_B:
				keyName.append(" (Game B)");
				break;
			case GAME_C:
				keyName.append(" (Game C)");
				break;
			case GAME_D:
				keyName.append(" (Game D)");
				break;
			default:
				break;
			}
			if (Boolean.TRUE.equals(keysPressed.get(keyCode))) {
				keyName.append("*");
			}

			g.setColor(colorHLFore);
			g.drawString(
					keyName.toString(),
					width / 2, (height * (2 * i - 1)) / (2 * size) + 1,
					Graphics.BASELINE | Graphics.HCENTER);
		}
	}

	private void drawPointsPressed(final Graphics g) {
		g.setColor(colorFore);

		if (firstPoint != null) {
			g.fillArc(firstPoint.x - 1, firstPoint.y - 1, 3, 3, 0, 360);
		}

		if (!pointsDragged.isEmpty()) {
			final Enumeration e = pointsDragged.elements();
			while (e.hasMoreElements()) {
				final Point p = (Point) e.nextElement();
				g.fillRect(p.x - 1, p.y - 1, 3, 3);
			}
		}

		if (lastPoint != null) {
			g.drawArc(lastPoint.x - 3, lastPoint.y - 3, 6, 6, 0, 360);
		}
	}

	private static final class Point {
		public final int x, y;

		public Point(final int x, final int y) {
			this.x = x;
			this.y = y;
		}

		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}

			final Point other = (Point) obj;
			return this.x == other.x && this.y == other.y;
		}

		public int hashCode() {
			int hash = 3;
			hash = 19 * hash + this.x;
			hash = 19 * hash + this.y;
			return hash;
		}
	}

	public void commandAction(final Command c, final Displayable d) {
		if (c == fullscreenCommand) {
			isFullscreenMode = !isFullscreenMode;
			setFullScreenMode(isFullscreenMode);
		} else if (c == toggleTickerCommand) {
			setTicker(getTicker() == null ? ticker : null);
		}
	}
}
