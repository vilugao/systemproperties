import javax.microedition.lcdui.*;

final class FontTestForm extends Form
implements CommandListener, ItemStateListener {
	private final Command updateCommand = new Command("Update", Command.OK, 0);

	private final ChoiceGroup faceChoice;
	{
		faceChoice = new ChoiceGroup(
			"Face: ", Choice.EXCLUSIVE,
			new String[]{"System", "Monospace", "Proportional"}, null);
		faceChoice.setSelectedIndex(0, true);
	}
	private final int[] choiceToFontFace = {
		Font.FACE_SYSTEM, Font.FACE_MONOSPACE, Font.FACE_PROPORTIONAL,
	};

	private final ChoiceGroup styleChoice = new ChoiceGroup(
			"Style: ", Choice.MULTIPLE,
			new String[]{"Bold", "Italic", "Underlined"}, null);

	private final Gauge sizeGauge = new Gauge("Size: ", true, 2, 1);
	private final int[] gaugeToFontSize = {
		Font.SIZE_SMALL, Font.SIZE_MEDIUM, Font.SIZE_LARGE,
	};

	private final StringItem sampleSI = new StringItem(
			"Sample: ", "Hamburgefonstiv");

	private final StringItem heightSI = new StringItem(
			"Height: ", Font.getDefaultFont().getHeight() + " px");

	public FontTestForm(final String title, final Display display) {
		super(title);
		append(faceChoice);
		append(styleChoice);
		append(sizeGauge);
		if (SystemPropertiesMIDlet.IS_MIDP_2) {
			append(sampleSI);
		}
		append(heightSI);
		addCommand(updateCommand);
		setCommandListener(this);
		if (SystemPropertiesMIDlet.IS_MIDP_2) {
			setItemStateListener(this);
		}
	}

	public void itemStateChanged(final Item i) {
		final int face = choiceToFontFace[faceChoice.getSelectedIndex()];
		final int style
				= (styleChoice.isSelected(0) ? Font.STYLE_BOLD : 0)
				| (styleChoice.isSelected(1) ? Font.STYLE_ITALIC : 0)
				| (styleChoice.isSelected(2) ? Font.STYLE_UNDERLINED : 0);
		final int size = gaugeToFontSize[sizeGauge.getValue()];
		final Font font = Font.getFont(face, style, size);
		if (SystemPropertiesMIDlet.IS_MIDP_2) {
			sampleSI.setFont(font);
		}
		heightSI.setText(font.getHeight() + " px");
	}

	public void commandAction(final Command c, final Displayable d) {
		if (c == updateCommand) {
			itemStateChanged(null);
		}
	}
}
