import java.util.Enumeration;
import java.util.Hashtable;

import javax.bluetooth.LocalDevice;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.lcdui.*;
import javax.microedition.m3g.Graphics3D;
import javax.microedition.midlet.MIDlet;

/**
 * A MIDlet presenting the system properties.
 * <p>
 * Date Created: 2010-08-05.
 *
 * @author Vi Lugão
 * @version 2.3
 * @since 1.0
 */
public final class SystemPropertiesMIDlet extends MIDlet
implements CommandListener, ItemCommandListener {

	/** Command to exit application. */
	private final Command exitCommand
			= new Command("Exit", Command.EXIT, 1);

	/** Command to switch to displayable previously shown. */
	private final Command backCommand
			= new Command("Back", Command.BACK, 1);

	/** Command to show more properties. */
	private final Command moreCommand
			= new Command("More", Command.SCREEN, 1);

	/** Item command to follow the hyperlink. */
	private final Command goCommand
			= new Command("Go", Command.ITEM, 1);

	/** Item command to play flashing backlight or to vibrate. */
	private final Command playCommand
			= new Command("Play", Command.ITEM, 1);

	/** Flash backlight duration in milliseconds. Defined as {@value} ms. */
	private static final int FLASH_BACKLIGHT_MS = 5000;

	/** Vibration duration in milliseconds. Defined as {@value} ms. */
	private static final int VIBRATION_DURATION_MS = 1000;

	private StringItem flashButton;
	private StringItem vibrateButton;

	/** Reference for MIDlet display. */
	private Display display;

	/** A main menu selection of categories. */
	private final List categoryList = new List(
			getAppProperty("MIDlet-Name"), List.IMPLICIT,
			SystemPropertiesModel.MAIN_CATEGORY_STRINGS, null);

	/** A menu selection of subcategories. */
	private List subCategoryList;

	/** A form showing properties listing within category selected. */
	private Form propsForm;

	/* ============ Controller ============ */

	protected void startApp() {
		System.out.println("startApp()");
		if (display == null) {
			categoryList.addCommand(exitCommand);
			categoryList.setCommandListener(this);
			display = Display.getDisplay(this);
			display.setCurrent(categoryList);
		}
	}

	protected void pauseApp() {
		System.out.println("pauseApp()");
	}

	protected void destroyApp(final boolean unconditional) {
		System.out.println("destroyApp(" + unconditional + ")");
	}

	public void commandAction(final Command c, final Item i) {
		if (c == goCommand) {
			try {
				if (i instanceof StringItem
						&& platformRequest(((StringItem) i).getText())) {
					notifyDestroyed();
				}
			} catch (final ConnectionNotFoundException e) {
				display.setCurrent(new Alert(
						"URL not handled",
						e.getMessage(),
						null, AlertType.ERROR));
			}
		} else if (c == playCommand) {
			if (i == flashButton) {
				if (!display.flashBacklight(FLASH_BACKLIGHT_MS)) {
					display.setCurrent(new Alert(
							"Flash backlight function",
							"Flashing not supported.",
							null, AlertType.WARNING));
				}
			} else if (i == vibrateButton) {
				if (!display.vibrate(VIBRATION_DURATION_MS)) {
					display.setCurrent(new Alert(
							"Vibration function",
							"Vibration not supported.",
							null, AlertType.WARNING));
				}
			}
		}
	}

	public void commandAction(final Command c, final Displayable d) {
		if (c == exitCommand) {
			notifyDestroyed();
		} else if (c == backCommand) {
			if (subCategoryList != null && d != subCategoryList) {
				display.setCurrent(subCategoryList);
			} else {
				display.setCurrent(categoryList);
				subCategoryList = null;
			}
		} else if (
			c == List.SELECT_COMMAND
			&& (d == categoryList || d == subCategoryList)
		) {
			onSelectCategory();
		} else if (c == moreCommand) {
			onClickMoreProperties();
		} else if (d instanceof CommandListener) {
			((CommandListener) d).commandAction(c, d);
		}
	}

	/**
	 * Shows the properties of category selected in {@link #categoryList}
	 * or {@link #subCategoryList}, depending what is shown.
	 */
	private void onSelectCategory() {
		final int index = categoryList.getSelectedIndex();

		final String title;
		final String[] sysPropsRef;
		if (subCategoryList != null && subCategoryList.isShown()) {
			final int subindex = subCategoryList.getSelectedIndex();
			title = SystemPropertiesModel
					.getSubcategoryStrings(index)[subindex];
			sysPropsRef = SystemPropertiesModel
					.getSystemProperties(index, subindex);
		} else {
			title = SystemPropertiesModel.MAIN_CATEGORY_STRINGS[index];
			if (!SystemPropertiesModel.hasSubcategory(index)) {
				sysPropsRef = SystemPropertiesModel
						.getSystemProperties(index, -1);
			} else {
				sysPropsRef = null;
			}
		}

		final Displayable displayable;
		if (sysPropsRef == null) {
			subCategoryList = new List(
					title, List.IMPLICIT,
					SystemPropertiesModel.getSubcategoryStrings(index), null);
			displayable = subCategoryList;
		} else if (sysPropsRef == SystemPropertiesModel.LCDUI_FONTS_SYSPROPS) {
			displayable = new FontTestForm(title, display);
		} else if (sysPropsRef == SystemPropertiesModel.LCDUI_CANVAS_SYSPROPS) {
			displayable = new TestCanvas(display);
		} else {
			propsForm = new Form(title);
			if (sysPropsRef == SystemPropertiesModel.ABOUT_SYSPROPS) {
				appendAboutItems(propsForm);
			} else if (
				sysPropsRef == SystemPropertiesModel.LCDUI_DISPLAY_SYSPROPS
			) {
				appendDisplayProperties(propsForm);
			} else {
				for (int i = 0; i < sysPropsRef.length; i++) {
					final String key = sysPropsRef[i];
					propsForm.append(new StringItem(
							shortenPropertyKey(key) + ": ",
							System.getProperty(key)));
				}
				if (sysPropsRef == SystemPropertiesModel.BT_SYSPROPS
						|| sysPropsRef == SystemPropertiesModel.M3G_SYSPROPS) {
					propsForm.addCommand(moreCommand);
				}
			}
			displayable = propsForm;
		}

		displayable.addCommand(backCommand);
		displayable.setCommandListener(this);
		display.setCurrent(displayable);
	}

	static String shortenPropertyKey(final String key) {
		if (key.startsWith("microedition.")) {
			return "(me)" + key.substring(12);
		} else if (key.startsWith("bluetooth.")) {
			return "(bt)" + key.substring(9);
		} else if (key.startsWith("com.nokia.")) {
			return "(nk)" + key.substring(9);
		} else if (key.startsWith("com.sonyericsson.")) {
			return "(se)" + key.substring(16);
		}
		return key;
	}

	static final boolean IS_MIDP_2;
	static {
		final String midp = System.getProperty("microedition.profiles");
		IS_MIDP_2 = (midp != null) && midp.startsWith("MIDP-2.");
	}

	private void appendDisplayProperties(final Form form) {
		form.append(new StringItem(
				"Has color: ", String.valueOf(display.isColor())));
		form.append(new StringItem(
				"Number of colors: ", String.valueOf(display.numColors())));

		if (IS_MIDP_2) {
			form.append(new StringItem(
					"Alpha levels: ",
					String.valueOf(display.numAlphaLevels())));
			form.append(new StringItem(
					"Image size (alert): ",
					display.getBestImageWidth(Display.ALERT)
					+ " × "
					+ display.getBestImageHeight(Display.ALERT)));
			form.append(new StringItem(
					"Image size (choice group element): ",
					display.getBestImageWidth(Display.CHOICE_GROUP_ELEMENT)
					+ " × "
					+ display.getBestImageHeight(
							Display.CHOICE_GROUP_ELEMENT)));
			form.append(new StringItem(
					"Image size (list element): ",
					display.getBestImageWidth(Display.LIST_ELEMENT)
					+ " × "
					+ display.getBestImageHeight(Display.LIST_ELEMENT)));

			flashButton = new StringItem(
					"Flash backlight: ", "Play", Item.BUTTON);
			flashButton.setDefaultCommand(playCommand);
			flashButton.setItemCommandListener(this);
			form.append(flashButton);

			vibrateButton = new StringItem(
					"Vibrate: ", "Play", Item.BUTTON);
			vibrateButton.setDefaultCommand(playCommand);
			vibrateButton.setItemCommandListener(this);
			form.append(vibrateButton);
		}
	}

	private void onClickMoreProperties() {
		final String[] sysPropsRef = SystemPropertiesModel.getSystemProperties(
				categoryList.getSelectedIndex(),
				subCategoryList.getSelectedIndex());

		final Form form;
		if (sysPropsRef == SystemPropertiesModel.BT_SYSPROPS) {
			form = new Form("LocalDevice.getProperty()");
			for (int i = 0; i < sysPropsRef.length; i++) {
				final String key = sysPropsRef[i];
				form.append(new StringItem(
						shortenPropertyKey(key) + ": ",
						LocalDevice.getProperty(key)));
			}
		} else if (sysPropsRef == SystemPropertiesModel.M3G_SYSPROPS) {
			form = createGraphics3DPropertiesForm();
		} else {
			return;
		}

		form.addCommand(backCommand);
		form.setCommandListener(this);
		display.setCurrent(form);
	}

	private static Form createGraphics3DPropertiesForm() {
		final Hashtable properties = Graphics3D.getProperties();
		final Form form = new Form("Graphics3D.getProperties()");
		form.append(new StringItem(
				"Graphics3D.getProperties(): ",
				properties.size() + " item(s)."));
		final Enumeration keys = properties.keys();
		while (keys.hasMoreElements()) {
			final Object key = keys.nextElement();
			form.append(new StringItem(
					key.toString() + ": ",
					properties.get(key).toString()));
		}
		return form;
	}

	private void appendAboutItems(final Form form) {
		form.append(
				getAppProperty("MIDlet-Name") + " "
				+ getAppProperty("MIDlet-Version") + " by "
				+ getAppProperty("MIDlet-Vendor") + ".\n");
		form.append(getAppProperty("MIDlet-Description") + '\n');
		form.append("The \"(me).\" prefix mean \"microedition.\";"
				+ " \"(bt).\" mean \"bluetooth.\";"
				+ " \"(nk).\" mean \"com.nokia.\" and"
				+ " \"(se).\" mean \"com.sonyericsson.\".\n");

		final Runtime rt = Runtime.getRuntime();
		form.append(new StringItem("Total memory: ", rt.totalMemory() + " B"));
		form.append(new StringItem("Free memory: ", rt.freeMemory() + " B"));
	}
}
